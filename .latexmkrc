#!/usr/bin/env perl
#$latex            = 'platex -kanji=utf-8 -synctex=1 -halt-on-error %S';
$latex            = 'ptex2pdf -l -ot "-synctex=1 -file-line-error" %S';
$bibtex           = 'pbibtex';
$biber            = 'biber  -u -U --output_safechars';
$dvipdf           = 'dvipdfmx %O -o %D %S';
$makeindex        = 'mendex %O -o %D %S';
$max_repeat       = 5;
$pdf_mode         = 3;
$pvc_view_file_via_temporary = 0;
$pdf_previewer = "open -ga /Applications/Skim.app";
