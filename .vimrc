"dein Scripts-----------------------------
if &compatible
	set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/Users/yume_yu/.cache/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/Users/yume_yu/.cache/dein')
	call dein#begin('/Users/yume_yu/.cache/dein')

	" Let dein manage dein
	" Required:
	call dein#add('/Users/yume_yu/.cache/dein/repos/github.com/Shougo/dein.vim')


	" Add or remove your plugins here:
	if !has('nvim')
		call dein#add('roxma/nvim-yarp')
		call dein#add('roxma/vim-hug-neovim-rpc')
	endif
	" プラグインリストを収めた TOML ファイル
	let g:rc_dir    = expand('~/.vim/rc')
	let s:toml      = g:rc_dir . '/dein.toml'
	let s:lazy_toml = g:rc_dir . '/plugins_lazy.toml'
	" TOML を読み込み、キャッシュしておく
	call dein#load_toml(s:toml,      {'lazy': 0})
	call dein#load_toml(s:lazy_toml, {'lazy': 1})

	" Required:
	call dein#end()
	call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
	call dein#install()
endif

"End dein Scripts-------------------------"
set number	" 行番号

"pythonの場所の指定
let g:python3_host_prog = expand('/Users/yume_yu/.pyenv/versions/3.6.0/bin/python')
" TABキーを押した際にタブ文字の代わりにスペースを入れる
set expandtab
set tabstop=2
set shiftwidth=2
autocmd FileType python      setlocal tabstop=4 shiftwidth=4
autocmd BufNewFile,BufRead *.c setlocal  dictionary=~/c.dict
autocmd BufNewFile,BufRead *.h setlocal  dictionary=~/c.dict
set cindent
set backspace=indent,eol,start	"Ins時のbackspaseの挙動をいつもどおりにする
set background=dark
set whichwrap=b,s,h,l,<,>,[,],~ " カーソルの左右移動で行末から次の行の行頭への移動が可能になる

"エンコードの設定
set encoding=utf-8
"ファイルのエンコードの設定
set fileencodings=utf-8,ucs-bom,iso-2022-jp-3,iso-2022-jp,eucjp-ms,euc-jisx0213,euc-jp,sjis,cp932,
"ファイルの改行絡み
set fileformat=unix
set fileformats=unix,dos,mac
"カーソル行のハイライト表示
set cursorline

"折りたたみ有効化
set foldmethod=indent
" マルチバイト文字の表示時のズレを解消
"if exists('&ambiwidth')       " ○等の記号でカーソル位置がずれないように
"  set ambiwidth=double
"endif

"カラースキーム固定
colorscheme iceberg
"背景色をターミナルに統一
highlight Normal ctermbg=none
highlight LineNr ctermbg=none
highlight Nontext ctermbg=none

"ファイル変更履歴 .netrwhistを生成しないようにする
:let g:netrw_dirhistmax = 0

set guifont=Hack\ Regular\ Nerd\ Font\ Complete:16
" Required for operations modifying multiple buffers like rename.
set hidden
autocmd BufNewFile,BufRead *.swift set filetype=swift

set list
set listchars=tab:»-,trail:-,nbsp:%,eol:↲
"クリップボード有効化
set clipboard+=unnamedplus
