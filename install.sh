#!/bin/sh

#vim
ln -sf ~/dotfiles/.vim ~/.vim
ln -sf ~/dotfiles/.vimrc ~/.vimrc

#neovim
ln -sf ~/dotfiles/.vim ~/.config/nvim
ln -sf ~/dotfiles/.vimrc ~/.config/nvim/init.vim

#editorconfig
ln -sf ~/dotfiles/.editorconfig.origin ~/.editorconfig.origin

#latexmkrc
ln -sf ~/dotfiles/.latexmkrc ~/.latexmkrc
